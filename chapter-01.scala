/* 1 */
println("http://www.scala-lang.org/api/current/#scala.Int")

/* 2. */
import math._
println(3 - pow(sqrt(3), 2))

/* 3. */
println("res in scala REPL is val")

/* 4. */
println("crazy*3")
println("http://www.scala-lang.org/api/current/#scala.collection.immutable.StringOps")
println("""
   def *(n: Int): String
      Return the current string concatenated n times. 
""")

/* 5. */
println(10 max 3)
println("http://www.scala-lang.org/api/current/#scala.Int")
println("""
   def max(that: Int): Int
      Returns this if this > that or that otherwise.
""")

/* 6. */
val v:BigInt = 2
println( v pow 1024)

/* 7. */
import BigInt._
import util.Random
println( probablePrime(100,Random))

/* 8. */
import BigInt._
import util.Random
val bigInt:BigInt = probablePrime(100,Random)
println(bigInt.toString(36))
println("""
   def toString(radix: Int): String

      Returns the String representation in the specified radix of this BigInt.
""")

/* 9. */
val str = "first things first"
println(str(0))	
println(str.last)

/* 10. */
println("""
   def take(n: Int): String

      Selects first n elements.

   def takeRight(n: Int): String

      Selects last n elements.

   def drop(n: Int): String

      Selects all elements except first n ones.

   def dropRight(n: Int): String

      Selects all elements except last n ones.
""")