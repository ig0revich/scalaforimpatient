/* 1. */
def phillArray(n:Int):Array[Int]={ 
   var arr = new Array[Int](n); 
   for(i <- 0 until n) 
      arr(i)=i; 
   arr
}

println( 
   phillArray(5).mkString("[",",","]") + "\n"
)

/* 2. */
def toggle(arr:Array[Int]):Array[Int]={
   for(i <- 0 until arr.length if 0<i && 0!=i%2 ){
         val tmp  = arr(i)
         arr(i)   = arr(i-1)
         arr(i-1) = tmp      
   }
   arr   
}

println(
   toggle(Array(1,2,3,4,5)).mkString("[",",","]") + "\n"
)

println(
   toggle(Array(1,2,3,4)).mkString("[",",","]") + "\n"
) 

/* 3. */
val arr:Array[Int] = Array(1,2,3,4,5,6)

def toggleYield(arr:Array[Int]):Array[Int]={
   (for(i <- 0 until arr.length)
      yield {
         if(i<arr.length-1 && 0==i%2){
	    val tmp = arr(i)
	    arr(i) = arr(i+1)
	    arr(i+1) = tmp
         }
         arr(i)
      }   
   ).toArray   
}

println(
   arr.mkString("[",",","]") + "\n"
)

println(
   toggleYield(arr).mkString("[",",","]") + "\n"
) 

(for(a <- arr.grouped(2); b<-a.reverse) yield b).toArray

arr.grouped(2).flatMap(_.reverse).toArray

/* 4. */
val arr1 = Array(-2,11,456,0,-6,-8,0,77,0,1)
println(
   arr1.filter(_>0) ++ arr1.filter(_<0) ++ arr1.filter(_==0)
)   

/* 5. */
val arr2:Array[Double] = Array(550.78,110001.0,30000.01)
println( arr2.sum / arr2.size )

/* 6. */
val arr3 = Array(44,-100,1,77,0,-2)
util.Sorting.quickSort(arr3)

import collection.mutable._
val buf = ArrayBuffer(44,-100,1,77,0,-2)
buf.sorted

/* 7. */
val buf1 = Array(44,-100,1,77,0,-2,-100,1,1,1)
buf1 distinct

/* 8. (удалить все отрицательные числа кроме первого: собрать индексы отрицательных элементов, 
расположить их в обратном порядке, отбросить последний индекс, для
каждого индекса вызвать a.remove(i) */
val arr4 = ArrayBuffer(-4,-5,1,77,101,-8)

( for(i <- 0 until arr4.length if 0 > arr4(i) ) yield i )
   .reverse
   .dropRight(1)
   .foreach( arr4.remove(_) )
   
println(arr4.mkString(","))


/* 9. */
import collection.JavaConversions.asScalaBuffer
val zones = java.util.TimeZone.getAvailableIDs
val america = (for{zone<-zones if(zone.startsWith("America/"))} yield zone.split("/")(1)).sorted
println(america.mkString(","))

/* 10. */
import java.awt.datatransfer._
import collection.JavaConversions.asScalaBuffer
import collection.mutable.Buffer
val flavors = SystemFlavorMap.getDefaultFlavorMap.asInstanceOf[SystemFlavorMap]
val res: Buffer[String] = flavors.getNativesForFlavor(DataFlavor.imageFlavor)
println(res)

