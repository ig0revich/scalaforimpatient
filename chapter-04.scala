/* 1. */
println ( for((k,v)<- Map("iPhone 6S"->16999.00, "iPad Mini 4"->10299.00)) yield (k,v-v/10) )

/* 2. */
val in = new java.util.Scanner(new java.io.File("words.txt"))
val counters = collection.mutable.Map[String,Int]()
while(in.hasNext()) { 
   val word = in.next
   counters(word)=counters.getOrElse(word,0) + 1       
}

println(counters)

/* 3. */
val in1 = new java.util.Scanner(new java.io.File("words.txt"))
var counters1 = collection.immutable.Map[String,Int]()
while(in1.hasNext()) { 
   val word = in1.next
   counters1 = counters1 + ( word -> (1+counters1.getOrElse(word,0)) )
}

println(counters1)

/* 4. */
val in2 = new java.util.Scanner(new java.io.File("words.txt"))
var counters2 = collection.immutable.SortedMap[String,Int]()
while(in2.hasNext()) { 
   val word = in2.next
   counters2 = counters2 + ( word -> (1+counters2.getOrElse(word,0)) )
}

for((k,v)<-counters2) println(k,v)
