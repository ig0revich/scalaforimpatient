/* 1. */
def signum(d:Int)={
  if(d>0)
	 1
  else
  if(d<0)
	 -1
  else     
	 0
}
println(signum(-100))

/* 2. */
val empty = {}
println(empty)

/* 3. */
var x=()
var y=0
x=y=1

/* 4. */
for(i <- 10 to 0 by -1)
 println(i)
 
/* 5. */
def countdown(n:Int){
  for(i <- n to 0 by -1)
	 println(i)
}
countdown (5)

/* 6. */
def unixCodeMultiply(s:String):Long = {
   var acc:Long = 1;
   for(ch <- s) acc*=ch
   acc
}
println(unixCodeMultiply("Hello"))

/* 7. */
var out:Long = 1;
"Hello".foreach((ch)=>out*=ch)
println(out)

/* 8. */
def product(s : String)={
   var out:Long = 1;
   s.foreach((ch)=>out*=ch)
   out
}
println( product("Hello") )

/* 9. */
def productRecursive(s:String):Long={
   if(s.isEmpty) 
      1
   else
      s.head * productRecursive(s.tail)    
}
println( productRecursive("Hello") )

/* 10. */
def _pow(x:Double, n:Double):Double = {
      if(n>0){
 	     if(0==n%2)
	        _pow(x,n/2)*_pow(x,n/2)
	     else	 	  
            x*(_pow(x, n-1))
	  }else
	  if(n==0)
	     1
      else
         1/_pow(x, -n)  
   	  
}
println(_pow(2,-2))
println(_pow(3,3))
